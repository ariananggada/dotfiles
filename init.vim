" for neovim, placed in ~/.config/nvim/init.vim
" bootstrap for load settings from ~/.vimrc
" settings default path for vim
" this set make neovim and vim happy:)

    set runtimepath^=~/.vim runtimepath+=~/.vim/after
    let &packpath = &runtimepath
    source ~/.vimrc
