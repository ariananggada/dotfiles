# ARIAN ANGGADA
# 2018/09/11

#!/usr/bin/env bash


setup_gitprompt() {
  if [ ! -e ~/.git-prompt.sh ]; then
    curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh -o ~/.git-prompt.sh
  fi
}

setup_tmux() {
  echo "Setting up tmux..." \
  && rm -rf ~/.tmux.conf ~/.tmux \
  && mkdir ~/.tmux \
  && ln -sf $(pwd)/tmux.conf ~/.tmux.conf \
  && git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm \
  && ~/.tmux/plugins/tpm/bin/install_plugins \
  && tmux source ~/.tmux.conf
}

setup_vim() {
  export GIT_SSL_NO_VERIFY=true
  mkdir -p ~/.vim/autoload
  mkdir -p ~/.vim/colors
  ln -sf $(pwd)/vimrc ~/.vimrc
  ln -sf $(pwd)/vim/colors/ian.vim ~/.vim/colors/ian.vim
}

setup_neovim() {
  ln -sf $(pwd)/init.vim ~/.config/nvim/init.vim
}

setup_ranger() {
  mkdir -p ~/.config/ranger
  ln -sf $(pwd)/config/ranger/commands.py ~/.config/ranger/commands.py
  ln -sf $(pwd)/config/ranger/rifle.conf ~/.config/ranger/rifle.conf

}

install_ctags() {
  local ctags_installed=$(which ctags)
  if [[ -z $ctags_installed ]]; then
    echo "Installing universal ctags..." \
    && rm -rf ./ctags \
    && git clone https://github.com/universal-ctags/ctags \
    && cd ctags && ./autogen.sh && ./configure && make && sudo make install && cd ../ && rm -rf ctags
  fi
}

echo -n "This will delete all your previous vim, tmux settings. Proceed? (y/n)? "
read answer
if echo "$answer" | grep -iq "^y" ;then
  echo "Installing dependencies..." \
  && setup_tmux \
  && setup_vim \
  && setup_neovim \
  && setup_ranger \
  && install_ctags \
  && echo "Finished installation."
fi
