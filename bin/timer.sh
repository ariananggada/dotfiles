if [ -z "$1" ]
  then
    echo "./timer.sh (time, ex: 1s, 1m, 1h)"
    exit 1 
fi
~/bin/timer.py $1 && ffplay -nodisp -autoexit ~/bin/sound/win_95.wav >/dev/null 2>&1

