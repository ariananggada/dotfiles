docker run -d -p 9999:9000 -v /var/run/docker.sock:/var/run/docker.sock \
  -v portainer_data:/data --name portainer --restart always portainer/portainer
