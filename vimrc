filetype plugin indent on
syntax on

if ! filereadable(expand('~/.vim/autoload/plug.vim'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ~/.vim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ~/.vim/autoload/plug.vim
endif

let mapleader=','

call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdtree'
Plug 'junegunn/goyo.vim'
Plug 'PotatoesMaster/i3-vim-syntax'
Plug 'LukeSmithxyz/vimling'
Plug 'vimwiki/vimwiki'
Plug 'bling/vim-airline'
Plug 'tpope/vim-commentary'
Plug 'vifm/vifm.vim'
Plug 'kovetskiy/sxhkd-vim'
Plug 'tpope/vim-unimpaired'
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-repeat'

" Color scheme
Plug 'morhetz/gruvbox'

" coc nvim
Plug 'neoclide/coc.nvim', {'tag': '*', 'do': { -> coc#util#install({'tag':1})}}

" golang plugin
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

" gitgutter
Plug 'airblade/vim-gitgutter'

" git wrapper
Plug 'tpope/vim-fugitive'

" rest client
Plug 'diepm/vim-rest-console'

" auto ctags
Plug 'ludovicchabant/vim-gutentags'

" tag bar
Plug 'majutsushi/tagbar', { 'on': 'TagbarToggle' }

" markdown preview
Plug 'suan/vim-instant-markdown', {'for': 'markdown'}

" Task Wiki ( taskwarrior + vimwiki )
Plug ('tbabej/taskwiki')

call plug#end()

if has('termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set t_Co=256
  set background=dark
  set termguicolors
endif

if v:version >= 703
  set undodir=/tmp//,.
endif

set nocompatible
set background=dark
set guioptions=all
set mouse=a
set clipboard=unnamedplus
set hidden
set expandtab
set path=.,**
set nobackup
set nowritebackup
set noswapfile
set nojoinspaces
set cmdheight=2
set updatetime=500
set timeoutlen=500
set signcolumn=yes
set tags=./tags;,tags;
set shiftwidth=2
set ignorecase smartcase
set complete-=i
set nostartofline
set encoding=utf-8
set number relativenumber
set wildmenu
set wildmode=full

" optim
set lazyredraw
set nocursorline
set ttyfast

if exists('&colorcolumn')
  set colorcolumn=80
endif
set textwidth=0

" EX | chmod +x
" Function
command! EX if !empty(expand('%'))
         \|   write
         \|   call system('chmod +x '.expand('%'))
         \|   silent e
         \| else
         \|   echohl WarningMsg
         \|   echo 'Save the file first'
         \|   echohl None
         \| endif

" <Leader>?/! | Google it / Feeling lucky
" Function
function! s:goog(pat, lucky)
  let q = '"'.substitute(a:pat, '["\n]', ' ', 'g').'"'
  let q = substitute(q, '[[:punct:] ]',
       \ '\=printf("%%%02X", char2nr(submatch(0)))', 'g')
  call system(printf('xdg-open "https://www.google.com/search?%sq=%s"',
                   \ a:lucky ? 'btnI&' : '', q))
endfunction
nnoremap <leader>? :call <SID>goog(expand("<cWORD>"), 0)<cr>
nnoremap <leader>! :call <SID>goog(expand("<cWORD>"), 1)<cr>
xnoremap <leader>? "gy:call <SID>goog(@g, 0)<cr>gv
xnoremap <leader>! "gy:call <SID>goog(@g, 1)<cr>gv

" Some basics mapping
nnoremap c "_c
inoremap jk <Esc>

" abbreviation
      iab meday <C-R>=strftime('%a %d %b %y')<CR>
      iab medate <C-R>=strftime('%Y-%m-%d')<CR>
      iab metime <C-R>=strftime('%H:%M:%S')<CR>

" Scratch Buffer
      command! SC vnew | setlocal nobuflisted buftype=nofile bufhidden=wipe noswapfile

" Enable autocompletion:
      set wildmode=longest,list,full

" Disables automatic commenting on newline:
      autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Goyo plugin makes text more readable when writing prose:
      nnoremap <Space>g :Goyo \| set bg=light \| set linebreak<CR>

" Gruvbox option
      let g:gruvbox_contrast_dark='soft'

" Spell-check set to <leader>o, 'o' for 'orthography':
      map <leader>o :setlocal spell! spelllang=en_us<CR>

" Splits open at the bottom and right, which is non-retarded, unlike vim defaults.
      set splitbelow splitright


" Coc.nvim
" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-Space> to trigger completion.
inoremap <silent><expr> <c-Space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[c` and `]c` to navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" use `:OR` for organize import of current buffer
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Using CocList
" Show all diagnostics
nnoremap <silent> <Space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <Space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <Space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <Space>o  :<C-u>CocList outline<cr>
" Search workSpace symbols
nnoremap <silent> <Space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <Space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <Space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <Space>p  :<C-u>CocListResume<CR>

" prettier
command! -nargs=0 Prettier :CocCommand prettier.formatFile

" end of coc.nvim config


" Fzf.nvim
      if has('nvim') || has('gui_running')
              let $FZF_DEFAULT_OPTS .= ' --inline-info'
      endif

      let g:fzf_tags_command = 'ctags -R'

      command! -bang -nargs=* Rg
        \ call fzf#vim#grep(
        \   'rg --column --line-number --no-heading --color=always --smart-case '.shellescape(<q-args>), 1,
        \   <bang>0 ? fzf#vim#with_preview('up:60%')
        \           : fzf#vim#with_preview('right:50%:hidden', '?'),
        \   <bang>0)

      command! -bang -nargs=? -complete=dir Files
      \ call fzf#vim#files(<q-args>, fzf#vim#with_preview(), <bang>0)

      nnoremap <Space><Space> :Files<CR>
      nmap <Space><tab> <plug>(fzf-maps-n)
      xmap <Space><tab> <plug>(fzf-maps-x)


" Nerd tree
      map <leader>n :NERDTreeToggle<CR>
      nnoremap <silent> <expr> <Leader><Leader> (expand('%') =~ 'NERD_tree' ? "\<c-w>\<c-w>" : '').":Files\<cr>"
      autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Tagbar
      let g:tagbar_sort = 0

" instant markdown
let g:instant_markdown_slow = 1
let g:instant_markdown_autostart = 0
let g:instant_markdown_open_to_the_world = 1
let g:instant_markdown_allow_unsafe_content = 1
let g:instant_markdown_allow_external_content = 1


" vimling:
      nm <leader>d :call ToggleDeadKeys()<CR>
      imap <leader>d <esc>:call ToggleDeadKeys()<CR>a
      nm <leader>i :call ToggleIPA()<CR>
      imap <leader>i <esc>:call ToggleIPA()<CR>a
      nm <leader>q :call ToggleProse()<CR>

" Shortcutting split navigation, saving a keypress:
      map <C-h> <C-w>h
      map <C-j> <C-w>j
      map <C-k> <C-w>k
      map <C-l> <C-w>l

" indent with > and < char via visual mode without removing selection
      vnoremap > >gv
      vnoremap < <gv

" Check file in shellcheck:
      map <leader>s :!clear && shellcheck %<CR>

" " Open my bibliography file in split
" 	map <leader>b :vsp<Space>$BIB<CR>
" 	map <leader>r :vsp<Space>$REFER<CR>

" Buffer list mapping
" Require FZF
      nnoremap <Space>b :Buffer<CR>
" Default buffer command
      nnoremap <leader>b :buffer *
" Delete buffer
      nnoremap <Space>d :bd<cr>

" Tags mapping
" require FZF
      nnoremap <Space>t :Tags<CR>

" Ripgrep mapping
" require FZF
      nnoremap <Space>r :Rg<CR>

" Black hole delete
      nnoremap <leader>d ""

" Find file
      nnoremap <Space>f :find *

" Update or save file
      nnoremap <Space>s :update<cr>
      nnoremap <Space>w :write<cr>

" Quit from vim
      nnoremap <Space>q :q<cr>

" Vertical split find
      nnoremap <Space>v :vert sfind *

" Replace all is aliased to S.
      nnoremap S :%s//g<Left><Left>

" More manageable brace expansions
      inoremap (; (<CR>);<C-c>O
      inoremap (, (<CR>),<C-c>O
      inoremap {; {<CR>};<C-c>O
      inoremap {, {<CR>},<C-c>O
      inoremap [; [<CR>];<C-c>O
      inoremap [, [<CR>],<C-c>O

" Compile document, be it groff/LaTeX/markdown/etc.
      map <leader>c :w! \| !compiler <c-r>%<CR>

" Open corresponding .pdf/.html or preview
      map <leader>p :!opout <c-r>%<CR><CR>

" Runs a script that cleans out tex build files whenever I close out of a .tex file.
      autocmd VimLeave *.tex !texclear %

" Ensure files are read as what I want:
      let g:vimwiki_ext2syntax = {'.Rmd': 'markdown', '.rmd': 'markdown','.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
      let g:vimwiki_list = [{'path': '~/vimwiki', 'syntax': 'markdown', 'ext': '.md'}]
      autocmd BufRead,BufNewFile /tmp/calcurse*,~/.calcurse/notes/* set filetype=markdown
      autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff
      autocmd BufRead,BufNewFile *.tex set filetype=tex

" Copy selected text to system clipboard (requires gvim/nvim/vim-x11 installed):
      vnoremap <C-c> "+y
      map <C-p> "+P

" Enable Goyo by default for mutt writting
	" Goyo's width will be the line limit in mutt.
      autocmd BufRead,BufNewFile /tmp/neomutt* let g:goyo_width=80
      autocmd BufRead,BufNewFile /tmp/neomutt* :Goyo \| set bg=light

" Automatically deletes all trailing whiteSpace on save.
      autocmd BufWritePre * %s/\s\+$//e

" Automatically highlight all trailing whiteSpace
      highlight RedundantSpaces ctermbg=red guibg=red
      match RedundantSpaces /\s\+$/

" When shortcut files are updated, renew bash and vifm configs with new material:
      autocmd BufWritePost ~/.config/bmdirs,~/.config/bmfiles !shortcuts

" Update binds when sxhkdrc is updated.
      autocmd BufWritePost *sxhkdrc !pkill -USR1 sxhkd

" Run xrdb whenever Xdefaults or Xresources are updated.
      autocmd BufWritePost *Xresources,*Xdefaults !xrdb %

"""LATEX
	" Word count:
      autocmd FileType tex map <leader>w :w !detex \| wc -w<CR>
	" Code snippets
      autocmd FileType tex inoremap ,fr \begin{frame}<Enter>\frametitle{}<Enter><Enter><++><Enter><Enter>\end{frame}<Enter><Enter><++><Esc>6kf}i
      autocmd FileType tex inoremap ,fi \begin{fitch}<Enter><Enter>\end{fitch}<Enter><Enter><++><Esc>3kA
      autocmd FileType tex inoremap ,exe \begin{exe}<Enter>\ex<Space><Enter>\end{exe}<Enter><Enter><++><Esc>3kA
      autocmd FileType tex inoremap ,em \emph{}<++><Esc>T{i
      autocmd FileType tex inoremap ,bf \textbf{}<++><Esc>T{i
      autocmd FileType tex vnoremap , <ESC>`<i\{<ESC>`>2la}<ESC>?\\{<Enter>a
      autocmd FileType tex inoremap ,it \textit{}<++><Esc>T{i
      autocmd FileType tex inoremap ,ct \textcite{}<++><Esc>T{i
      autocmd FileType tex inoremap ,cp \parencite{}<++><Esc>T{i
      autocmd FileType tex inoremap ,glos {\gll<Space><++><Space>\\<Enter><++><Space>\\<Enter>\trans{``<++>''}}<Esc>2k2bcw
      autocmd FileType tex inoremap ,x \begin{xlist}<Enter>\ex<Space><Enter>\end{xlist}<Esc>kA<Space>
      autocmd FileType tex inoremap ,ol \begin{enumerate}<Enter><Enter>\end{enumerate}<Enter><Enter><++><Esc>3kA\item<Space>
      autocmd FileType tex inoremap ,ul \begin{itemize}<Enter><Enter>\end{itemize}<Enter><Enter><++><Esc>3kA\item<Space>
      autocmd FileType tex inoremap ,li <Enter>\item<Space>
      autocmd FileType tex inoremap ,ref \ref{}<Space><++><Esc>T{i
      autocmd FileType tex inoremap ,tab \begin{tabular}<Enter><++><Enter>\end{tabular}<Enter><Enter><++><Esc>4kA{}<Esc>i
      autocmd FileType tex inoremap ,ot \begin{tableau}<Enter>\inp{<++>}<Tab>\const{<++>}<Tab><++><Enter><++><Enter>\end{tableau}<Enter><Enter><++><Esc>5kA{}<Esc>i
      autocmd FileType tex inoremap ,can \cand{}<Tab><++><Esc>T{i
      autocmd FileType tex inoremap ,con \const{}<Tab><++><Esc>T{i
      autocmd FileType tex inoremap ,v \vio{}<Tab><++><Esc>T{i
      autocmd FileType tex inoremap ,a \href{}{<++>}<Space><++><Esc>2T{i
      autocmd FileType tex inoremap ,sc \textsc{}<Space><++><Esc>T{i
      autocmd FileType tex inoremap ,chap \chapter{}<Enter><Enter><++><Esc>2kf}i
      autocmd FileType tex inoremap ,sec \section{}<Enter><Enter><++><Esc>2kf}i
      autocmd FileType tex inoremap ,ssec \subsection{}<Enter><Enter><++><Esc>2kf}i
      autocmd FileType tex inoremap ,sssec \subsubsection{}<Enter><Enter><++><Esc>2kf}i
      autocmd FileType tex inoremap ,st <Esc>F{i*<Esc>f}i
      autocmd FileType tex inoremap ,beg \begin{DELRN}<Enter><++><Enter>\end{DELRN}<Enter><Enter><++><Esc>4k0fR:MultipleCursorsFind<Space>DELRN<Enter>c
      autocmd FileType tex inoremap ,up <Esc>/usepackage<Enter>o\usepackage{}<Esc>i
      autocmd FileType tex nnoremap ,up /usepackage<Enter>o\usepackage{}<Esc>i
      autocmd FileType tex inoremap ,tt \texttt{}<Space><++><Esc>T{i
      autocmd FileType tex inoremap ,bt {\blindtext}
      autocmd FileType tex inoremap ,nu $\varnothing$
      autocmd FileType tex inoremap ,col \begin{columns}[T]<Enter>\begin{column}{.5\textwidth}<Enter><Enter>\end{column}<Enter>\begin{column}{.5\textwidth}<Enter><++><Enter>\end{column}<Enter>\end{columns}<Esc>5kA
      autocmd FileType tex inoremap ,rn (\ref{})<++><Esc>F}i

""".bib
      autocmd FileType bib inoremap ,a @article{<Enter>author<Space>=<Space>{<++>},<Enter>year<Space>=<Space>{<++>},<Enter>title<Space>=<Space>{<++>},<Enter>journal<Space>=<Space>{<++>},<Enter>volume<Space>=<Space>{<++>},<Enter>pages<Space>=<Space>{<++>},<Enter>}<Enter><++><Esc>8kA,<Esc>i
      autocmd FileType bib inoremap ,b @book{<Enter>author<Space>=<Space>{<++>},<Enter>year<Space>=<Space>{<++>},<Enter>title<Space>=<Space>{<++>},<Enter>publisher<Space>=<Space>{<++>},<Enter>}<Enter><++><Esc>6kA,<Esc>i
      autocmd FileType bib inoremap ,c @incollection{<Enter>author<Space>=<Space>{<++>},<Enter>title<Space>=<Space>{<++>},<Enter>booktitle<Space>=<Space>{<++>},<Enter>editor<Space>=<Space>{<++>},<Enter>year<Space>=<Space>{<++>},<Enter>publisher<Space>=<Space>{<++>},<Enter>}<Enter><++><Esc>8kA,<Esc>i

"MARKDOWN
      nnoremap <leader>1 m`yypVr=``
      nnoremap <leader>2 m`yypVr-``
      nnoremap <leader>3 m`^i### <esc>``4l
      nnoremap <leader>4 m`^i#### <esc>``5l
      nnoremap <leader>5 m`^i##### <esc>``6l


" autoreload vimrc
      augroup vimrc
        autocmd! bufwritepost .vimrc source %
      augroup end

" colorscheme settings
if has('gui_running')
  set guifont=Inconsolata:h14 columns=80 lines=40
  silent! colo gruvbox
else
  silent! colo gruvbox
endif
