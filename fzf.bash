# new settings with minpac
# link to ~/.fzf.bash
# ln -sf `pwd`/fzf.bash ~/.fzf.bash
# Setup fzf
# ---------
if [[ ! "$PATH" == */home/$USER/.vim/pack/minpac/start/fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/home/$USER/.vim/pack/minpac/start/fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/$USER/.vim/pack/minpac/start/fzf/shell/completion.bash" 2> /dev/null

# Key bindings
# ------------
source "/home/$USER/.vim/pack/minpac/start/fzf/shell/key-bindings.bash"
