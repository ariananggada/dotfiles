MD_FILES=about.md 130-final-notes.md general-advice.md requirements.md \
         software-processes.md modeling.md architectural-design.md \
         design-of-components.md software-quality.md \
         configuration-management.md testing.md week-2-discussion.md \
         week-3-discussion.md week-4-discussion.md
PDF_FILES=$(MD_FILES:.md=.pdf)
BUILD_PDF_FILES=$(PDF_FILES:%=build/%)
EXTRA_PDFS=sample-midterm-solutions.pdf

130.pdf: $(BUILD_PDF_FILES)
        gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/default \
                -dNOPAUSE -dQUIET -dBATCH -dDetectDuplicateImages
                -dCompressFonts=true -r150 -sOutputFile=$@ $^ $(EXTRA_PDFS)

build/%.pdf: %.md
        @mkdir -p $$(dirname $@)
        pandoc -V geometry:margin=1in -o $@ $?
