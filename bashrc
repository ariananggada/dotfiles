# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac


# HIST* are bash-only variables, not environmental variables, so do not 'export'
# ignoredups only ignores _consecutive_ duplicates.
HISTCONTROL=erasedups:ignoreboth
HISTSIZE=20000
HISTFILESIZE=20000
HISTIGNORE='exit:cd:ls:bg:fg:history:f:fd'
HISTTIMEFORMAT='%F %T '
# append to the history file, don't overwrite it
shopt -s histappend
PROMPT_COMMAND='history -a' # append history file after each command
# truncate long paths to ".../foo/bar/baz"
PROMPT_DIRTRIM=4

# update $LINES and $COLUMNS after each command.
shopt -s checkwinsize
# (bash 4+) enable recursive glob for grep, rsync, ls, ...
shopt -s globstar &> /dev/null

#disable ctrl-s (scroll-lock) and ctrl-q
command -v stty > /dev/null 2>&1 && stty -ixon -ixoff

# Display matches for ambiguous patterns at first tab press
bind "set show-all-if-ambiguous on"

is_in_path() {
  echo "${PATH}:" | 2>&1 >/dev/null grep -E "${HOME}/bin"'/?:'
}

path_prepend() {
  [ -z "$1" ] && { echo 'path_prepend: missing/empty arg' ; exit 1 ; }
  if ! is_in_path "$1" && [ -d "$1" ] ; then
    PATH="${1}:${PATH}"
  fi
}

# Add these dirs to $PATH.
path_prepend "${HOME}/bin/ctags/bin"
path_prepend "${HOME}/dasht/bin"
path_prepend "${HOME}/bin"

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# msysgit cygwin
# if [[ $TERM == 'cygwin' && $OSTYPE == 'msys' ]] ; then
if [ "$MSYSTEM" = MINGW32 ]; then
    export LS_COLORS='di=01;36'
    alias pt='pt --nocolor'
fi


# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi


PS1="$(date +%m%d.%H%M) \[\033[0;32m\]\u@\h \[\033[36m\]\w\[\033[0m\] 
$ "


if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
fi


function bd () {
  local old_dir=`pwd`
  local new_dir=`echo $old_dir | sed 's|\(.*/'$1'[^/]*/\).*|\1|'`
  index=`echo $new_dir | awk '{ print index($1,"/'$1'"); }'`
  if [ $index -eq 0 ] ; then
    echo "No such occurrence."
  else
    echo $new_dir
    cd "$new_dir"
  fi
}

# provide 'watch' for systems that do not have it.
#   passive tail approach: http://stackoverflow.com/a/9574526/152142
if ! command -v watch > /dev/null 2>&1 ; then
  function watch() {
    while sleep 1; do
      # clear screen if possible
      command -v clear > /dev/null 2>&1 && clear
      $*
    done
  }
fi

#msysgit cygwin sets this, even over ssh; full cygwin sets this to 'xterm'.
if [[ $TERM != 'cygwin' ]]; then
    alias tmux='tmux -2'
fi

if grep --color "a" <<< "a" &> /dev/null ; then
    alias grep='grep --color=auto'
fi

alias cclip='xclip -selection clipboard'
#alias vi='nvim'
#alias vim='nvim'

# my current configuration for git alias
alias g='git status'
alias ga='git add'
alias gba='git branch --all'
alias gd='git diff --color --color-words --abbrev'
alias gcm='git commit'
alias gco='git checkout'
alias gf='git fetch'
alias gpl='git pull'
alias gps='git push'
alias cd..='cd ..'
alias k9='kill -9 %%'
alias tmux='tmux -2'


# echo "nameserver 127.0.0.1" >> /etc/resolv.conf
export EDITOR=nvim
export VISUAL=nvim


export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# man documentation with nvim
command -v nvim > /dev/null 2>&1 && export MANPAGER="nvim +Man!"

export GOPATH=$HOME/go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin

export PATH=$PATH:~/.local/bin

# RVM
source ~/.rvm/scripts/rvm

xrdb ~/.Xresources

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# matrix fun
alias rmatrix='echo -ne "\e[31m" ; while true ; do echo -ne "\e[$(($RANDOM % 2 + 1))m" ; tr -c "[:print:]" " " < /dev/urandom | dd count=1 bs=50 2> /dev/null ; done'
alias gmatrix='echo -ne "\e[32m" ; while true ; do echo -ne "\e[$(($RANDOM % 2 + 1))m" ; tr -c "[:print:]" " " < /dev/urandom | dd count=1 bs=50 2> /dev/null ; done'
