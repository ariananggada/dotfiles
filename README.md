# MIGRATING TO https://github.com/ariananggada/dotfiles


# Neovim and terminal configuration

This is my vim / neovim editor setup, with tmux configurations.
Feel free to fork it
and submit a pull request if you found any bug.

## Installation
-----------

````
$ git clone https://gitlab.com/ariananggada/dotfiles.git ~/dotfiles
$ cd ~/dotfiles
$ chmod +x ./install.sh
$ ./install.sh

yay done:)
````

## Tmux Cheatsheet

````
&lt;C-SPACE&gt; = &lt;leader&gt;
&lt;leader&gt;c = create new window
&lt;leader&gt;w = list window
&lt;Alt-n&gt; = split pane vertically
&lt;Alt-o&gt; = split pane horizontally
&lt;leader&gt;p = go-to prev window
&lt;leader&gt;n = go-to next window
````
## Vim cheatsheet 
~~~~
&lt;SPACE&gt;           = &lt;leader&gt;
&lt;leader&gt;&lt;leader&gt;  = FZF
&lt;leader&gt;v         = edit ~/.vimrc

under cursor to open file
&lt;g&gt;&lt;x&gt;      = open multimedia,document file
~~~~

## Apps
* mps-youtube(youtube cli) \
`install mps-youtube and mpv(player)`
after that, run : 
`mpsyt` \
or \
`dotfiles/bin/youtube.sh`
* dnscrypt
* noisy
* pomodoro \
`require ffmpeg package(ffplay)`

