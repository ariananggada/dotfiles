#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*


if [ -f /etc/profile ]; then
  PATH=
  source /etc/profile
fi

. ~/.bashrc

export GPG_TTY=$(tty)
